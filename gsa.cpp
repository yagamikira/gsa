#include<iostream>
#include<cstdlib>
#include<ctime>
#include<algorithm>
#include<cstring>
#include<vector>
using namespace std;
int main()
{
	srand(time(NULL));
	double G=1000000,e=2;
	double x[100],f[100],v[100],a[100],m[100],F[100];
	bool xx[100];
	for(int i=0;i<100;i++)
	{
		x[i]=rand()%10001-5000;
		xx[i]=true;
		f[i]=0;
		v[i]=0;
		a[i]=0;
		F[i]=0;
	}
	cout<<"enter coefficient of your function in the form of y=ax^n+bx^(n-1)+cx^(n-2)....\n";
	cout<<"Enter only coeffient from first to last such that n should be even and 'a' should be positive \n";
	cout<<"Enter n:-";
	int n;
	cin>>n;
	vector<double>function;
	for(int i=n;i>=0;i--)
	{
		double y;
		cout<<"Enter the coefficient of x^"<<i<<" :- ";
		cin>>y;
		function.push_back(y);
	}
	if((n%2)||(function[0]<=0))
	{
		cout<<"INVALID INPUT...";
		return 0;
	}
	for(int i=0;i<100;i++)
	{
		for(int j=0;j<=n;j++)
			f[i]=(f[i]*x[i])+function[j];
	}
	while(1)
	{
		int qq=0;
		double best=0,worst=0;
		for(int i=0;i<n;i++)
		{
			if(xx[i])
			{
				if(qq==0)
				{
					qq=1;
					best=worst=f[i];
				}
				else
				{
					if(f[i]<best)
						best=f[i];
					else if(f[i]>worst)
						worst=f[i];
				}
			}
		}
		if(worst-best<=0.01)
			break;
		double total=0;
		for(int i=0;i<100;i++)
		{
			m[i]=worst-f[i];
			total+=m[i];
			if(m[i]<=0.01)
				xx[i]=false;
		}
//		for(int i=0;i<100;i++)
//			m[i]/=total;
		G/=e;
		for(int i=0;i<100;i++)
			F[i]=0;
		for(int i=0;i<100;i++)
		{
			if(xx[i]==false)
				continue;
			for(int j=i+1;j<100;j++)
			{
				if(!xx[j])
					continue;
				double force=G*0.5*(m[i]*m[j])/((x[i]-x[j])*(x[i]-x[j]));
				if(x[i]<x[j])
				{
					F[i]-=(force/m[i]);
					F[j]+=(force/m[j]);
				}
				else
				{
					F[j]-=(force/m[j]);
					F[i]+=(force/m[i]);
				}
			}
		}
		for(int i=0;i<100;i++)
		{
			if(xx[i])
			{
				v[i]+=a[i];
				x[i]+=v[i];
				double aa=0;
				for(int j=0;j<=n;j++)
					aa=(aa*x[i])+function[j];
				if(aa<f[i])
					f[i]=aa;
			}
		}
	}
	double ans=1e12;
	for(int i=0;i<100;i++)
		ans=min(ans,f[i]);
	cout<<ans;
	return 0;
}
